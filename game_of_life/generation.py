from game_of_life.survival import *
import numpy as np

def generation(univers):
    "Applique les règles du jeu de la vie à chaque cellule et renvoie l'univers à la génération n+1"
    size = len(univers),len(univers[0])
    new_universe = np.zeros(size)
    for x in range (size[0]):
        for y in range (size[1]):
            if univers[x][y]==1:
                new_universe[x][y] = survival(univers, (x,y))
            else:
                new_universe[x][y] = naissance(univers, (x,y))
    return new_universe


