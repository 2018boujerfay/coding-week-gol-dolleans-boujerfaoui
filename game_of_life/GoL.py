import argparse as ap
from game_of_life.GoL_simulate import *

parser = ap.ArgumentParser()
parser.add_argument("size", type=tuple, help="display a universe of the given size")
parser.add_argument("type_seed", type=str, help="start universe with a seed of the given type")
parser.add_argument("iterations", type=int)
args = parser.parse_args()
simulate(args[0], args[1], args[2])
