from game_of_life.generate_universe import generate_universe
from game_of_life.add_seed import create_seed,add_seed_to_universe
from game_of_life.survival import *
from game_of_life.generation import *
from game_of_life.GoL_simulate import simulate
import numpy as np

def test_generate_universe():
    assert generate_universe((4,4)) == np.array([[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]).all

def test_create_seed():
    seed = create_seed(type_seed = "r_pentomino")
    assert seed == [[0,1,1],[1,1,0],[0,1,0]]
    seed = create_seed(type_seed= "acorn")
    assert seed == [
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0, 0, 0, 0],
        [0, 1, 1, 0, 0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0]]


def test_add_seed_to_universe():
    seed = create_seed(type_seed = "r_pentomino")
    universe = generate_universe(size=(6,6))
    universe = add_seed_to_universe(seed, universe,x_start=1, y_start=1)
    test_equality=np.array(universe ==np.array([[0, 0, 0, 0, 0, 0], [0, 0, 1, 1, 0, 0], [0, 1, 1, 0, 0, 0], [0 ,0, 1, 0, 0, 0], [0 ,0, 0, 0, 0, 0], [0 ,0, 0, 0, 0, 0]],dtype=np.uint8))
    assert test_equality.all()

def test_survival():
    seed = create_seed(type_seed="r_pentomino")
    universe = generate_universe(size = (6,6))
    universe = add_seed_to_universe(seed, universe, 1,1)
    test1 = survival(universe, (2,2))
    test2 = survival(universe, (1,3))
    assert test1 == 0
    assert test2 == 1

def test_naissance():
    seed = create_seed(type_seed="r_pentomino")
    universe = generate_universe(size = (6,6))
    universe = add_seed_to_universe(seed, universe, 1,1)
    test1 = naissance(universe, (0,0))
    test2 = naissance(universe, (3,2))
    assert test1 == 0
    assert test2 == 0
    universe = generate_universe(size = (10,10))
    seed = create_seed("acorn")
    universe = add_seed_to_universe(seed, universe, 0,0)
    assert naissance(universe, (2,2)) == 1


def test_generation():
    seed = create_seed(type_seed="r_pentomino")
    universe = generate_universe(size = (6,6))
    universe = add_seed_to_universe(seed, universe, 1,1)
    assert np.array_equal(generation(universe), [[0, 0, 0, 0, 0, 0], [0, 1, 1, 1, 0, 0], [0, 1, 0, 0, 0, 0], [0, 1, 1, 0, 0, 0],[0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]])

test_create_seed()
test_survival()
test_naissance()
test_generation()
