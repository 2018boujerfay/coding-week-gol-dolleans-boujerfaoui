import matplotlib.pyplot as plt
from game_of_life.add_seed import *
def Affichage(universe):
    size = len(universe), len(universe[0])

    for y in range(size[0]):
        for x in range (size[1]):
            if universe[y][x] == 1:
                plt.fill([x, x+1, x+1, x],[y, y, y-1, y-1], "k")
    axes = plt.gca()
    axes.set_xlim(-1, size[0]+1)
    axes.set_ylim(-1, size[1]+1)
    plt.gca().invert_yaxis()
    plt.show()

if __name__=="__main__":
    Affichage(create_seed("acorn"))

