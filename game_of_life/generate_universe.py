import numpy as np

def generate_universe(size):
    "Crée un univers remplie de cellules mortes à partir de la donnée d'un couple représentant les dimensions"
    universe=np.array([[0 for j in range(0,size[0])]for i in range(0,size[1])])
    return universe


