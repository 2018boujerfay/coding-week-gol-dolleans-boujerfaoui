import numpy as np

seeds = {
    "boat": [[1, 1, 0], [1, 0, 1], [0, 1, 0]],
    "r_pentomino": [[0, 1, 1], [1, 1, 0], [0, 1, 0]],
    "beacon": [[1, 1, 0, 0], [1, 1, 0, 0], [0, 0, 1, 1], [0, 0, 1, 1]],
    "block_switch_engine": [
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 1, 1],
        [0, 0, 0, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [1, 0, 1, 0, 0, 0, 0, 0],
    ],
    "infinite": [
        [1, 1, 1, 0, 1],
        [1, 0, 0, 0, 0],
        [0, 0, 0, 1, 1],
        [0, 1, 1, 0, 1],
        [1, 0, 1, 0, 1],
    ],
    "acorn": [
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0, 0, 0, 0],
        [0, 1, 1, 0, 0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0]
    ],
    "block": [
        [1,1],
        [1,1]
    ],
    "beehive": [
        [0, 1, 1, 0],
        [1, 0, 0, 1],
        [0, 1, 1, 0]
    ],
    "glider": [
        [0, 1, 0],
        [0, 0, 1],
        [1, 1, 1]
    ]
}



def create_seed(type_seed):
    "Renvoie une graine du type indiqué"
    try:
        seed = seeds[type_seed]
        return seed
    except KeyError:
        print("Ce type de graine est inconnu")
    except:
        print("probleme lors de la création de la graine")



def add_seed_to_universe(seed, universe, x_start, y_start):

    size_seed = (len(seed),len(seed[0]))
    if size_seed[0]>len(universe) or size_seed[1]>len(universe[0]):
        print("la graine est trop grande")

    try:
        for x in range (size_seed[0]):
            for y in range (size_seed[1]):
                universe[x_start + x][y_start + y] = seed[x][y]
    except:
        print("la graine est trop grande")
    return universe



