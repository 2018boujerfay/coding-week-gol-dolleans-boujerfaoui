
import pygame
from pygame import *
import random as rd
import time

#couleur
RED = (255, 0, 0)
GREEN = (0, 255, 255)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

#création de la classe cellule
class cell:
    def __init__(self, état, x, y):
        self.etat = état
        self.voisin = 0
        self.x = x
        self.y = y
        
#création de la grille
def generate(size):
    """créer un univers rempli de cellule dans un état aléatoire de la taille donnée"""
    size_x, size_y = size
    #initialise l'univers
    universe = []
    for x in range (size_x):
        universe.append([])
        for y in range (size_y):
            #ajoute des cellules dans un état aléatoire à l'univers
            universe[x].append(cell(rd.randint(0,1), x, y))
    return universe

def wrap_around(dx, dy, size):
    """Si on sort de l'univers on revient de l'autre coté"""
    size_x, size_y = size
    if dy > size_y - 1:
        dy = 0
    elif dy < 0:
        dy = size_y -1
    if dx > size_x - 1:
        dx = 0
    elif dx < 0:
        dx = size_x - 1
    return dx, dy

def count_voisin(cell, universe):
    """place le nombre de voisin vivant de la cellule dans cell.voisin"""
    x, y = cell.x, cell.y
    size = len(universe), len(universe[0])
    for i in range(-1, 2):
        for j in range(-1, 2):
            if i !=0 or j!=0:
                dx, dy = wrap_around(x+i, y+j, size)
                cell.voisin += universe[dx][dy].etat


def update(universe):
    for x in range (len(universe)):
        for y in range (len(universe[0])):
            count_voisin(universe[x][y],universe)


def decide_colour_nxt_state(cell):
    """change l'état de la cellule si nécessaire et renvoie la couleur qui la rpz"""
    if cell.etat == 1:
        if cell.voisin == 2 or cell.voisin == 3:
            "la cellule survie, on reset le nombre de voisin vivant"
            cell.voisin = 0
            return BLACK
        else:
            "la cellule meurt"
            cell.voisin = 0
            cell.etat = 0
            return RED
    else:
        if cell.voisin == 3:
            "une cellule nait"
            cell.voisin = 0
            cell.etat = 1
            return GREEN
        else:
            cell.voisin = 0
            return WHITE

#main game function
def play():
        #initialization
        pygame.init()
        size_x = int(input("entrer la largeur de l'univers"))
        size_y = int(input("entrer la hauteur de l'univers"))
        size = size_x,size_y
        scrn = pygame.display.set_mode((500, 500))
        mainsrf = pygame.Surface((500, 500))
        mainsrf.fill(WHITE)
        universe = generate(size)
        time.sleep(1)
        taille_rect_x = 500/size_x
        taille_rect_y = 500/size_y
        while 1:
                #tracking quitting
                update(universe)
                for event in pygame.event.get():
                        if event.type == QUIT:
                                pygame.quit()
                                sys.exit()
                #drawing
                for x in range(size[0]):
                        for y in range(size[1]):
                            pygame.draw.rect(mainsrf, decide_colour_nxt_state(universe[x][y]), (x*taille_rect_x, y*taille_rect_y, taille_rect_x, taille_rect_y))
                
                scrn.blit(mainsrf, (0, 0))
                pygame.display.update()
                time.sleep(1)

if __name__ == "__main__":
    play()
