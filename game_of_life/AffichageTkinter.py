from tkinter import *
from game_of_life.generate_universe import *
from game_of_life.generation import *
from game_of_life.survival import *
from game_of_life.add_seed import *
from game_of_life.Affichage_universe import *
import random as rd

fenetre = Tk()
game_of_life = Toplevel(fenetre)
game_of_life.grid()
canvas = Canvas(game_of_life, width=500, height=500)
canvas.pack()

var_text = StringVar
ligne_texte = Entry(fenetre, textvariable=var_text, width=30)
ligne_texte.pack()

def create_a_grid():
    """create the grid for the graphic interface"""
    global universe
    print("création de la grille")
    universe = generate_universe((50,50))
    global zones #contient l'ensemble des zones representant des cellules
    zones = []
    for x in range(len(universe)):
        zones.append([])
        for y in range(len(universe[0])):
            if universe[x][y]==1:
                zones[x].append(canvas.create_rectangle(x*10,y*10,(x+1)*10,(y+1)*10), fill="black")
            else:
                zones[x].append(canvas.create_rectangle(x*10,y*10,(x+1)*10 , (y+1)*10, fill="white"))



def update_grid():
    """met la grille à jour """
    global universe
    for x in range(len(universe)):
        for y in range (len(universe[0])):
            if universe[x][y] == 0:
                if naissance(universe,(x,y)) == 1:
                    "colorier la case zones[x][y] en vert"
                    canvas.itemconfig(zones[x][y], fill="green")
                else:
                    "colorier la case zones[x][y] en banc"
                    canvas.itemconfig(zones[x][y], fill="white")
            else:
                if survival(universe,(x,y)) == 0:
                    "colorier la case zones[x][y] en rouge"
                    canvas.itemconfig(zones[x][y], fill="red")
                else:
                    "colorier la case zones[x][y] en noir"
                    canvas.itemconfig(zones[x][y], fill="black")
    universe = generation(universe)


def create_cell_on_click(event):
    """créer une cellule vivante la ou l'utilisateur clique"""
    global universe
    x = event.x//10
    y = event.y//10
    universe[x][y] = 1
    update_grid()

def add_seed_tk():
    """ajoute la graine donnée dans la fenetre principale"""
    print("la fonction est appelé")
    global universe
    type_seed = ligne_texte.get()
    seed = create_seed(type_seed)
    print(seed)
    x_start = rd.randint(0,50-len(seed))
    y_start = rd.randint(0,50-len(seed[0]))
    universe= add_seed_to_universe(seed, universe, x_start, y_start)
    update_grid()


button_add_seed = Button(fenetre, text="ajoute la graine",command=add_seed_tk)
button_add_seed.pack()

button_next_step = Button(fenetre, text="go to next gen",command=update_grid)
button_next_step.pack()


create_a_grid()
canvas.bind("<Button-1>", create_cell_on_click)


fenetre.mainloop()
