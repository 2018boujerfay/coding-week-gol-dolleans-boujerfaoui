from game_of_life.generate_universe import *
from game_of_life.add_seed import *
from game_of_life.survival import *
from game_of_life.generation import *
from game_of_life.Affichage_universe import *
from matplotlib import pyplot as plt
from matplotlib import animation
import random as rd

def simulate(size, type_seed, iterations):
    """Création de l'univers et ajout de la graine"""
    """Size : taille de l'univers , Type_Seed : Type de la graine , Iterations : Nombre d'iterations d'univers"""
    universe = generate_universe(size)
    seed = create_seed(type_seed)
    """Choix aléatoire des coordonnées de la position aléatoire de la cellule tout en haut en gauche de la graine"""

    """Gestion d'exceptions"""
    try:
        x_start = rd.randint(0,size[0]-len(seed))
        y_start = rd.randint(0,size[1]-len(seed[0]))
    except:
        print("la graine est trop grande pour l'univers")
    """Ajout de la graine dans l'univers"""
    universe = add_seed_to_universe(seed,universe,x_start,y_start)
    fig=plt.figure()
    im=plt.imshow(universe,cmap="Greys",animated=True)
    """cmap : Chaine de caractère de coloriation de la figure , animated : Variable booléene pour l'activation de l'animation"""
    """Fonction d'animation"""
    def animate(i):
        universe=im.get_array()
        im.set_array(generation(universe))
        return im,
    anim = animation.FuncAnimation(fig, animate, interval=500, frames=iterations,blit=True)
    """Interval : le nombre de millisecondes entre deux mise à jour de la figure"""
    plt.show()
"""Exécution de la fonction pour la grille beacon du dictionnaire seeds"""
simulate((6,6), "beacon", 10)


def main():
    x = int(input("Entrer la largeur de l'univers"))
    y = int(input("Entrer la hauteur de l'univers"))
    type_seed = input("Entrer le type de graine")
    iterations = int(input("Entrer le nombre d'itérations désiré"))
    simulate((x,y),type_seed, iterations)


if __name__ == "__main__":
    main()
