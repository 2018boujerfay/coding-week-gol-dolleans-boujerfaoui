def survival(universe, cell):
    "Le programme teste la survie de la cellule et modifie l'univers si elle meurt, et renvoie l'état future de la cellule"
    x, y = cell
    size = (len(universe),len(universe[0]))
    nombre_de_voisin_vivant = 0
    #le programme parcours tous les voisins de la cellule dont on teste la survie
    for i in range (-1,2):
        for j in range (-1,2):
            #vérifie que le voisin est bien dans l'univers
            if x+i >=0 and x+i< size[0]:
                if y+j >=0 and y+j< size[1]:
                    if not (i == 0 and j==0):
                        #compte le nombre de voisin vivant de la cellule
                        nombre_de_voisin_vivant += universe[x+i][y+j]
    if nombre_de_voisin_vivant == 2 or nombre_de_voisin_vivant == 3:
        #Si la cellule a 2 ou 3 voisins vivants elle survit
        return 1
    else:
        #Sinon elle meurt
        return 0


def naissance(universe, cell):
    "Teste si il y a naissance dans la cellule et renvoie l'état future de la cellule"
    x, y = cell
    size = (len(universe),len(universe[0]))
    nombre_de_voisin_vivant = 0
    #le programme parcours tous les voisins de la cellule dont on teste la survie
    for i in range (-1,2):
        for j in range (-1,2):
            #vérifie que le voisin est bien dans l'univers
            if x+i >=0 and x+i< size[0]:
                if y+j >=0 and y+j< size[1]:
                    if not (i == 0 and j==0):
                        #compte le nombre de voisin vivant de la cellule
                        nombre_de_voisin_vivant += universe[x+i][y+j]
    if nombre_de_voisin_vivant == 3:
        #Si la cellule a exactement 3 voisins vivants il y a naissance
        return 1
    return 0

